## Mysqldump.sh

This is a bash script to backup a specific MySQL database. It prompts for MySQL credentials, lists all databases available, prompts for the number of the database to dump, and then executes `mysqldump` to create a gziped backup file.

### Prerequisites

- Bash
- MySQL client installed

### Usage

1. Make sure you have execution permissions for the script file. If not, run `chmod +x mysqldump.sh` to add execution permissions.
2. Run the script by executing `./mysqldump.sh`.
3. When prompted, enter your MySQL username and password.
4. The script will list all databases available for backup, along with numbers.
5. Enter the number corresponding to the database you want to backup.
6. If the entered number is invalid or the selected database doesn't exist, the script will exit with an error message.
7. Once a valid database is selected, the script will execute `mysqldump` to create a backup of the chosen database.
8. The backup file will be compressed with gzip and saved with a filename format of `database.timestamp.sql.gz`, where `database` is the name of the chosen database and `timestamp` is the current date and time in the format `YYYY-MM-DDTHH:MM:SS`.
9. After the backup is completed, the script will display the path to the backup file.

**Note**: The script assumes that the `mysqldump` command is in your system's `PATH` variable. Make sure that you can access `mysqldump` from the command line before running the script.

### Example

```bash
$ ./mysqldump.sh
Enter MySQL username: myuser
Enter MySQL password:
Available databases:
1 example_db_1
2 example_db_2
Enter the number of the database to dump: 2
Database dump saved to example_db_2.2022-07-01T12:34:56.sql.gz
```