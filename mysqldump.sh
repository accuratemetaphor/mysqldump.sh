#!/bin/bash

# Prompt for MySQL credentials
read -p "Enter MySQL username: " username
read -s -p "Enter MySQL password: " password
echo

# List all databases with numbers
databases=$(mysql -u "$username" -p"$password" -e "SHOW DATABASES;" | awk 'NR>1{print NR-1,$0}' | grep -Ev "(Database|information_schema|performance_schema)")

if [ -z "$databases" ]; then
  echo "No databases found!"
  exit 1
fi

# Prompt for the number of the database to dump
echo "Available databases:"
echo "$databases"
read -p "Enter the number of the database to dump: " database_number

# Validate database number
if ! [[ "$database_number" =~ ^[0-9]+$ ]]; then
  echo "Invalid database number!"
  exit 1
fi

# Get the name of the database based on the number
database=$(echo "$databases" | awk -v number="$database_number" '$1==number{print $2}')

if [ -z "$database" ]; then
  echo "Invalid database number!"
  exit 1
fi

# Execute mysqldump and compress with gzip
timestamp=$(date +'%Y-%m-%dT%H:%M:%S')
filename="$database.$timestamp.sql.gz"
echo "Saving database dump to $filename"
mysqldump -u "$username" -p"$password" "$database" | gzip > "$filename"
echo "Database dump saved to $filename"
